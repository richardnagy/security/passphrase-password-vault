# PassPhrase Password Vault

## Usage

1. Create a new password file, or open an existing one by browsing, then providing a master password.

> ⚠️ In case of a lost master password, the data cannot be recovered

![login](images/login.png "Open or create password file")

2. Add your passwords to the file, then save them. You may also choose to unhide the username/email and password fields in case there is nobody near.

![passwords](images/passwords.png "Adding passwords")

![saving](images/saving.png "Saving")

3. After saving you may exit the application and the passwords are saved in the `.pass` file encrypted. You are only able to re-open it by supplying the master password.

![file](images/pass-file.png "File")

- You may also choose to export your passwords into a readable file. You can do this using the interactive formatter. The importer is able to read any output produced by the exporter.

![export](images/export.png "Export")

- You are also able to change the master password for the current database using the lock button on the left.